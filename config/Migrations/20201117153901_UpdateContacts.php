<?php
use Migrations\AbstractMigration;

class UpdateContacts extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $this->execute('ALTER TABLE contacts MODIFY id INT UNSIGNED NOT NULL AUTO_INCREMENT');
    }
}
