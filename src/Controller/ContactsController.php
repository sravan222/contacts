<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Contacts Controller
 *
 * @property \App\Model\Table\ContactsTable $Contacts
 *
 * @method \App\Model\Entity\Contact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContactsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        if ($this->getRequest()->is(['get'])) {
            $limit = 10;
            $page = 1;

            $queries = $this->getRequest()->getQuery();
            if (isset($queries['limit'])) {
                $limit = $queries['limit'];
            }

            if (isset($queries['page'])) {
                $page = $queries['page'];
            }

            $response = $this->Contacts->find()->select(['id', 'first_name', 'last_name', 'phone_number'])->limit($limit)->page($page)->toArray();

            $this->buildResponse($response, 200);
        } else {
            $response = 'Invalid Request';
            $this->buildResponse($response, 404);
        }

    }


    private function buildResponse($response, $httpStatus)
    {
        $this->viewBuilder()->setClassName('Json');

        $this->set('response', $response);
        $this->set('_serialize', 'response');

        $body = $this->response->getBody();
        $body->write(json_encode($response, JSON_UNESCAPED_UNICODE));
        $this->response = $this->response->withBody($body);
        $this->response = $this->response->withStatus($httpStatus);
        return $this->response;
    }


    /**
     * Contacts with Company Details
     */
    public function contactsWithCompanies()
    {
        $limit = 10;
        $page = 1;

        $queries = $this->getRequest()->getQuery();
        if (isset($queries['limit'])) {
            $limit = $queries['limit'];
        }

        if (isset($queries['page'])) {
            $page = $queries['page'];
        }

        $response = $this->Contacts->find()->contain(['Companies'])->select(['id', 'first_name', 'last_name', 'phone_number', 'Companies.id', 'Companies.company_name', 'Companies.address'])->limit($limit)->page($page)->toArray();

        $this->buildResponse($response, 200);
    }

    /**
     * Add Contact
     */
    public function add()
    {
        $contact = $this->Contacts->newEntity();
        $missingVariables = [];
        if ($this->request->is('post')) {
            $data = $this->request->input('json_decode');
            $contact->first_name = isset($data->first_name) ? $data->first_name : array_push($missingVariables, 'first_name');
            $contact->last_name = isset($data->last_name) ? $data->last_name : array_push($missingVariables, 'last_name');
            $contact->phone_number = isset($data->phone_number) ? $data->phone_number : array_push($missingVariables, 'phone_number');
            $contact->address = isset($data->address) ? $data->address : array_push($missingVariables, 'address');
            $contact->add_notes = isset($data->add_notes) ? $data->add_notes : array_push($missingVariables, 'add_notes');
            $contact->internal_notes = isset($data->internal_notes) ? $data->internal_notes : array_push($missingVariables, 'internal_notes');
            $contact->comments = isset($data->comments) ? $data->comments : array_push($missingVariables, 'comments');
            if (isset($data->company_id) && is_int($data->company_id)) {
                $contact->company_id = (int)$data->company_id;
            } else {
                $missingVariables[] = 'company_id';
            }

            if (count($missingVariables) < 0) {
                try {
                    if ($this->Contacts->save($contact)) {
                        $response = [];
                        $this->buildResponse($response, 200);
                    }
                } catch (\Exception $e) {
                    $this->buildResponse($e->getMessage(), 404);
                }
            } else {
                $response = implode(', ', $missingVariables) .  ((count($missingVariables) > 1) ? ' are ' : ' is ') . 'missing from the request';
                $this->buildResponse($response, 404);
            }

        } else {
            $response = 'Invalid Request';
            $this->buildResponse($response, 404);
        }
    }
}
